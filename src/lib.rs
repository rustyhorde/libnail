// Copyright (c) 2016 warheadhateus developers
//
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. All files in the project carrying such notice may not be copied,
// modified, or distributed except according to those terms.

//! libnail - MIO Handlers for use with nail and nailed.
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]
#![cfg_attr(feature="clippy", allow(stutter))]
#![deny(missing_docs)]
extern crate byteorder;
extern crate bytes;
#[macro_use]
extern crate lazy_static;
extern crate mio;
extern crate rand;
extern crate regex;
extern crate slab;
extern crate uuid;

// This needs to stay first, don't rearrange
#[macro_use]
mod utils;

mod client;
mod endpoint;
mod error;
mod receiver;
mod sender;
mod server;
mod ssh;
mod sshd;

pub use client::Client;
pub use endpoint::Udp;
pub use error::NailErr;
pub use receiver::Receiver;
pub use sender::Sender;
pub use server::{NailMessage, NailMessageHandler, Server};
pub use sshd::{SshDaemon, SshPacketHandler};

/// Convenience Result for use with `NailErr`.
pub type NailResult<T> = Result<T, NailErr>;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {}
}
