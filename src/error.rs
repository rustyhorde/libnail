//! Error Implementation for libnail
use std::error;
use std::fmt;
use std::io;

/// Errors thrown by libnail.
#[derive(Debug)]
pub enum NailErr {
    /// io::Error Wrapper
    Io(io::Error),
    /// Error thrown getting Token
    Token,
    /// FromUtf8Error wrapper.
    Utf8Error(::std::string::FromUtf8Error),
}

impl fmt::Display for NailErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            NailErr::Io(ref err) => err.fmt(f),
            NailErr::Token => write!(f, "Token lookup error"),
            NailErr::Utf8Error(ref err) => err.fmt(f),
        }
    }
}

impl error::Error for NailErr {
    fn description(&self) -> &str {
        match *self {
            NailErr::Io(ref err) => err.description(),
            NailErr::Token => "Token lookup error",
            NailErr::Utf8Error(ref err) => err.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            NailErr::Io(ref err) => Some(err),
            NailErr::Token => None,
            NailErr::Utf8Error(ref err) => Some(err),
        }
    }
}

impl From<io::Error> for NailErr {
    fn from(err: io::Error) -> NailErr {
        NailErr::Io(err)
    }
}

impl From<::std::string::FromUtf8Error> for NailErr {
    fn from(err: ::std::string::FromUtf8Error) -> NailErr {
        NailErr::Utf8Error(err)
    }
}
