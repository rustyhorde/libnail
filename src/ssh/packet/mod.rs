//! SSH Binary Packet Protocol
//! See https://tools.ietf.org/html/rfc4253#section-6
//! The packet contains the following fields.
//! - `packet_length`: The length of the packet in bytes, not including `mac` or the `packet_length`
//! field itself.  This is a u32.
//! - `padding_length`: Length of 'random padding' (bytes).  Must be between 4 and 255.
//! - `payload`: The useful contents of the packet.  If compression has been negotiated, this field
//! is compressed.  Initially, compression MUST be "none".
//! - `random_bytes`: Arbitrary-length padding, such that the total length of
//! (`packet_length` || `padding_length` || `payload` || `random_bytes`) is a multiple of the cipher
//! block size or 8, whichever is larger.  There MUST be at least four bytes of padding.  The
//! padding SHOULD consist of random bytes.  The maximum amount of padding is 255 bytes.
//! - `mac`: Message Authentication Code.  If message authentication has been negotiated, this field
/// contains the MAC bytes.  Initially, the MAC algorithm MUST be "none".
use ::NailResult;
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use error::NailErr::Token;
use rand::{self, Rng};
use rand::distributions::{IndependentSample, Range};
use ssh::{CipherBlockSize, KeyExchangeInit, MacAlgorithm};
use ssh::KeyExchangeAlgorithm::{self, OneSha1, FourteenSha1, ExchangeSha1, ExchangeSha256};
use std::io::Cursor;

pub mod kex;

#[cfg_attr(feature="clippy", allow(cast_possible_truncation))]
fn padding_length(payload_len: usize, cbs: u8) -> u8 {
    let block_size = cbs as usize;
    // Padding min is a number between 0 and (block_size - 1).  The 5 below represents the size in
    // bytes of the packet length value (u32 or 4 bytes) and the padding length value (1 byte).
    let padding_min = (block_size - 1) - (5 + payload_len) % block_size;
    // The minimum padding is 4, so set the min_factor to 1 in this case of padding_min < 4.
    let min_factor = if padding_min < 4 { 1 } else { 0 };
    // Setup some rng to pick a factor between (0 or 1) and (256 / block_size).
    let between = Range::new(min_factor, 256 / block_size);
    let mut rng = rand::thread_rng();
    // This calculates a padding length between 4 and 255
    (padding_min + (block_size * between.ind_sample(&mut rng))) as u8
}

#[derive(Default)]
pub struct SshPacket {
    kex_algo: Option<KeyExchangeAlgorithm>,
}

impl SshPacket {
    pub fn kex_algo(&mut self, kex_algo: Option<KeyExchangeAlgorithm>) -> &mut SshPacket {
        self.kex_algo = kex_algo;
        self
    }

    #[cfg_attr(feature="clippy", allow(cast_possible_truncation))]
    pub fn as_bytes(&self,
                    payload: Vec<u8>,
                    cph_blk_size: CipherBlockSize,
                    mac_algo: MacAlgorithm)
                    -> NailResult<Vec<u8>> {
        let payload_len = payload.len();
        let padding_len = padding_length(payload_len, cph_blk_size.into());
        let packet_len = (payload_len + padding_len as usize + 1) as u32;
        let mut padding = Vec::with_capacity(padding_len as usize);

        let mut rng = rand::thread_rng();
        for _ in 0..padding_len {
            padding.push(rng.gen::<u8>());
        }

        // Calculate the MAC here when implemented.
        let mac = match mac_algo {
            _ => vec![],
        };

        let mut packet = Vec::new();
        try!(packet.write_u32::<BigEndian>(packet_len));
        packet.push(padding_len);
        packet.extend_from_slice(&payload);
        packet.extend_from_slice(&padding);
        packet.extend_from_slice(&mac);
        Ok(packet)
    }

    pub fn into_payload(self, bytes: Vec<u8>) -> NailResult<SshPayload> {
        let mut bytes_iter = bytes.into_iter();
        // Get the Packet Length as u32
        let plv = bytes_iter.by_ref().take(4).collect::<Vec<u8>>();
        let mut rdr = Cursor::new(&plv);
        let packet_length = try!(rdr.read_u32::<BigEndian>());

        // Get the packet and mac.
        let packet = bytes_iter.by_ref().take(packet_length as usize).collect::<Vec<u8>>();
        let mut mac = Vec::new();
        mac.extend(bytes_iter);

        // TODO: Verify the MAC on packet and fail if it doesn't match.  Otherwise, generate
        // the SshPayload.

        // If the MAC is valid for the packet generate an SshPayload.
        let mut pkt_iter = packet.into_iter();
        // Get the Padding Length
        let padding_length = if let Some(pl) = pkt_iter.next() {
            pl
        } else {
            return Err(Token);
        };

        // Calculate the Payload Length
        let payload_length = packet_length - (padding_length as u32) - 1;

        // Grab the payload
        let payload = pkt_iter.by_ref().take(payload_length as usize).collect::<Vec<u8>>();
        Ok(SshPayload::new(payload, self.kex_algo))
    }
}

pub enum SshPayload {
    Disconnect,
    Ignore,
    Unimplemented,
    Debug,
    ServiceRequest,
    ServiceAccept,
    KeyExchangeInit(KeyExchangeInit),
    NewKeys,
    KeyExchangeDHInit,
    KeyExchangeDHReply,
    KeyExchangeDHGexRequestOld,
    KeyExchangeDHGexGroup,
    KeyExchangeDHGexInit,
    KeyExchangeDHGexReply,
    KeyExchangeDHGexRequest,
    Unknown,
}

impl SshPayload {
    pub fn new(bytes: Vec<u8>, kex_algo: Option<KeyExchangeAlgorithm>) -> SshPayload {
        let mut bytes_iter = bytes.clone().into_iter();
        let idv = bytes_iter.by_ref().take(4).collect::<Vec<u8>>();
        let mut rdr = Cursor::new(&idv);
        let id = rdr.read_u32::<BigEndian>();
        match (id, kex_algo) {
            (Ok(1), _) => SshPayload::Disconnect,
            (Ok(2), _) => SshPayload::Ignore,
            (Ok(3), _) => SshPayload::Unimplemented,
            (Ok(4), _) => SshPayload::Debug,
            (Ok(5), _) => SshPayload::ServiceRequest,
            (Ok(6), _) => SshPayload::ServiceAccept,
            (Ok(20), _) => {
                if let Ok(kexi) = KeyExchangeInit::from_bytes(bytes) {
                    SshPayload::KeyExchangeInit(kexi)
                } else {
                    SshPayload::Unknown
                }
            }
            (Ok(21), _) => SshPayload::NewKeys,
            (Ok(30), Some(OneSha1)) |
            (Ok(30), Some(FourteenSha1)) => SshPayload::KeyExchangeDHInit,
            (Ok(30), Some(ExchangeSha1)) |
            (Ok(30), Some(ExchangeSha256)) => SshPayload::KeyExchangeDHGexRequestOld,
            (Ok(31), Some(OneSha1)) |
            (Ok(31), Some(FourteenSha1)) => SshPayload::KeyExchangeDHReply,
            (Ok(31), Some(ExchangeSha1)) |
            (Ok(31), Some(ExchangeSha256)) => SshPayload::KeyExchangeDHGexGroup,
            (Ok(32), _) => SshPayload::KeyExchangeDHGexInit,
            (Ok(33), _) => SshPayload::KeyExchangeDHGexReply,
            (Ok(34), _) => SshPayload::KeyExchangeDHGexRequest,
            _ => SshPayload::Unknown,
        }
    }
}

#[cfg(test)]
mod test {
    use ssh::{CipherBlockSize, KeyExchangeInit, MacAlgorithm};
    use std::default::Default;
    use super::{padding_length, SshPacket};

    #[test]
    fn pad_len_eight() {
        // Assert that the padding length is always between 4 and 255 for an 8-byte block size.
        assert!((0..10000).map(|x| padding_length(x, 8)).min().unwrap() > 3);
    }

    #[test]
    fn pad_len_sixteen() {
        // Assert that the padding length is always between 4 and 255 for a 16-byte block size.
        assert!((0..10000).map(|x| padding_length(x, 16)).min().unwrap() > 3);
    }

    #[test]
    #[cfg_attr(feature="clippy", allow(use_debug))]
    fn new_packet() {
        let kex: KeyExchangeInit = Default::default();
        if let Ok(kex_bytes) = kex.into_bytes() {
            let ssh_packet: SshPacket = Default::default();
            if let Ok(p) =
                   ssh_packet.as_bytes(kex_bytes, CipherBlockSize::Eight, MacAlgorithm::NoMac) {
                stdout!(true, "{:?}", p);
                assert!(true);
            } else {
                assert!(false);
            }
        } else {
            assert!(false);
        }
    }
}
