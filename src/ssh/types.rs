use ::NailResult;
use regex::Regex;
use ssh::KeyExchangeInit;
use std::fmt;

pub enum CipherBlockSize {
    Eight,
    Sixteen,
}

impl From<u8> for CipherBlockSize {
    fn from(v: u8) -> CipherBlockSize {
        match v {
            16 => CipherBlockSize::Sixteen,
            _ => CipherBlockSize::Eight,
        }
    }
}

impl From<CipherBlockSize> for u8 {
    fn from(cbs: CipherBlockSize) -> u8 {
        match cbs {
            CipherBlockSize::Eight => 8,
            CipherBlockSize::Sixteen => 16,
        }
    }
}

#[derive(PartialEq)]
pub enum KeyExchangeAlgorithm {
    Curve25519Sha256,
    ExchangeSha1,
    ExchangeSha256,
    FourteenSha1,
    OneSha1,
}

use self::KeyExchangeAlgorithm::{Curve25519Sha256, ExchangeSha1, ExchangeSha256, FourteenSha1,
                                 OneSha1};

impl fmt::Display for KeyExchangeAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let kex_str = match *self {
            Curve25519Sha256 => "curve25519-sha256@libssh.org",
            ExchangeSha1 => "diffie-hellman-group-exchange-sha1",
            ExchangeSha256 => "diffie-hellman-group-exchange-sha256",
            FourteenSha1 => "diffie-hellman-group14-sha1",
            OneSha1 => "diffie-hellman-group1-sha1",
        };
        write!(f, "{}", kex_str)
    }
}

#[derive(PartialEq)]
pub enum ServerHostKeyAlgorithm {
    PgpSignDss,
    PgpSignRsa,
    SshDss,
    SshEd25519,
    SshRsa,
}

use self::ServerHostKeyAlgorithm::{PgpSignDss, PgpSignRsa, SshDss, SshEd25519, SshRsa};

impl fmt::Display for ServerHostKeyAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let shk_str = match *self {
            PgpSignDss => "pgp-sign-dss",
            PgpSignRsa => "pgp-sign-rsa",
            SshDss => "ssh-dss",
            SshRsa => "ssh-rsa",
            SshEd25519 => "ssh-ed25519",
        };
        write!(f, "{}", shk_str)
    }
}

#[derive(PartialEq)]
pub enum EncryptionAlgorithm {
    TripleDesCbc,
    BlowfishCbc,
    Twofish256Cbc,
    Twofish192Cbc,
    Twofish128Cbc,
    Aes256Cbc,
    Aes192Cbc,
    Aes128Cbc,
    Serpent256Cbc,
    Serpent192Cbc,
    Serpent128Cbc,
    Arcfour,
    IdeaCbc,
    Cast128Cbc,
    NoEnc,
}

use self::EncryptionAlgorithm::{TripleDesCbc, BlowfishCbc, Twofish256Cbc, Twofish192Cbc,
                                Twofish128Cbc, Aes256Cbc, Aes192Cbc, Aes128Cbc, Serpent256Cbc,
                                Serpent192Cbc, Serpent128Cbc, Arcfour, IdeaCbc, Cast128Cbc, NoEnc};

impl fmt::Display for EncryptionAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let enc_str = match *self {
            TripleDesCbc => "3des-cbc",
            BlowfishCbc => "blowfish-cbc",
            Twofish256Cbc => "twofish256-cbc",
            Twofish192Cbc => "twofish192-cbc",
            Twofish128Cbc => "twofish128-cbc",
            Aes256Cbc => "aes256-cbc",
            Aes192Cbc => "aes192-cbc",
            Aes128Cbc => "aes128-cbc",
            Serpent256Cbc => "serpent256-cbc",
            Serpent192Cbc => "serpent192-cbc",
            Serpent128Cbc => "serpent128-cbc",
            Arcfour => "arcfour",
            IdeaCbc => "idea-cbc",
            Cast128Cbc => "cast128-cbc",
            NoEnc => "none",
        };
        write!(f, "{}", enc_str)
    }
}

#[derive(PartialEq)]
pub enum MacAlgorithm {
    HmacSha1,
    HmacSha1NinetySix,
    HmacMd5,
    HmacMd5NinetySix,
    NoMac,
}

use self::MacAlgorithm::{HmacSha1, HmacSha1NinetySix, HmacMd5, HmacMd5NinetySix, NoMac};

impl fmt::Display for MacAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mac_str = match *self {
            HmacSha1 => "hmac-sha1",
            HmacSha1NinetySix => "hmac-sha1-96",
            HmacMd5 => "hmac-md5",
            HmacMd5NinetySix => "hmac-md5-96",
            NoMac => "none",
        };
        write!(f, "{}", mac_str)
    }
}

#[derive(PartialEq)]
pub enum CompressionAlgorithm {
    Xz,
    Zlib,
    NoComp,
}

use self::CompressionAlgorithm::{Xz, Zlib, NoComp};

impl fmt::Display for CompressionAlgorithm {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let comp_str = match *self {
            Xz => "xz",
            Zlib => "zlib",
            NoComp => "none",
        };
        write!(f, "{}", comp_str)
    }
}

pub const VERSION: Option<&'static str> = option_env!("CARGO_PKG_VERSION");

lazy_static! {
    pub static ref VER_EXCH_PROTO: Regex =
        Regex::new(r#"^SSH-2.0-([\x21-\x2C\x2E-\x7E]+)([ ][\x21-\x2C\x2E-\x7E]+)?\r\n$"#).unwrap();
}

#[derive(Default)]
pub struct VersionExchange {
    server_version: String,
    client_version: String,
}

impl VersionExchange {
    #[allow(dead_code)]
    pub fn client_version(&self) -> &String {
        &self.client_version
    }

    #[allow(dead_code)]
    pub fn server_version(&self) -> &String {
        &self.server_version
    }

    pub fn set_client_version(&mut self, client_version: String) -> &mut VersionExchange {
        self.client_version = client_version;
        self
    }

    pub fn set_server_version(&mut self, server_version: String) -> &mut VersionExchange {
        self.server_version = server_version;
        self
    }
}

impl fmt::Display for VersionExchange {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Server: {}Client: {}",
               self.server_version,
               self.client_version)
    }
}

#[derive(Default)]
pub struct KeyExchange {
    version_exchange: VersionExchange,
    server_key_exchange_init: Option<KeyExchangeInit>,
    client_key_exchange_init: Option<KeyExchangeInit>,
    key_exchange_algorithm: Option<KeyExchangeAlgorithm>,
    server_host_key_algorithm: Option<ServerHostKeyAlgorithm>,
    s2c_encryption_algorithm: Option<EncryptionAlgorithm>,
    c2s_encryption_algorithm: Option<EncryptionAlgorithm>,
    s2c_mac_algorithm: Option<MacAlgorithm>,
    c2s_mac_algorithm: Option<MacAlgorithm>,
    s2c_compression_algorithm: Option<CompressionAlgorithm>,
    c2s_compression_algorithm: Option<CompressionAlgorithm>,
}

impl KeyExchange {
    pub fn new(version_exchange: VersionExchange) -> KeyExchange {
        KeyExchange { version_exchange: version_exchange, ..Default::default() }
    }

    pub fn setup(&mut self,
                 ckex: KeyExchangeInit,
                 skex: KeyExchangeInit)
                 -> NailResult<&mut KeyExchange> {
        {
            let skexa = skex.kex_algorithms();
            let ckexa = ckex.kex_algorithms();
        }

        self.client_key_exchange_init = Some(ckex);
        self.server_key_exchange_init = Some(skex);
        Ok(self)
    }
}
#[cfg(test)]
mod test {
    use super::VER_EXCH_PROTO;

    #[test]
    fn version_exchange_regex() {
        assert!(VER_EXCH_PROTO.is_match("SSH-2.0-testclient_1.0\r\n"));
        assert!(VER_EXCH_PROTO.is_match("SSH-2.0-nailed_0.0.1 Prototype\r\n"));
        assert!(VER_EXCH_PROTO.is_match("SSH-2.0-nailed_0.0.1 Prototype_@Cool\r\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-1.0-someold\r\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-No-dashes-allowed\r\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-nailed_0.0.1 or spaces\r\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-or\ttabs\r\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-nailed_1.0.0 Or-dashes-here\r\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-nailed_1.0.0 Or spaces\r\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-nailed_1.0.0"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-nailed_1.0.0\n"));
        assert!(!VER_EXCH_PROTO.is_match("SSH-2.0-nailed_1.0.0\r"));
    }
}
