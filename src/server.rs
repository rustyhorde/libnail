//! nail server
use ::NailResult;
use bytes::RingBuf;
use bytes::traits::{Buf, MutBuf};
use error::NailErr;
use mio::{EventLoop, EventSet, Handler, PollOpt, Sender, Timeout, Token, TryRead, TryWrite};
use mio::tcp::{TcpListener, TcpStream};
use regex::Regex;
use slab::Slab;
use std::fmt;
use std::marker;
use std::net::SocketAddr;
use std::time::Duration;

lazy_static! {
    static ref MODE_STREAM: Regex = Regex::new(r"^mode stream ([0-9]+)$").unwrap();
}

/// Message Handler Trait for a Nail Server.
pub trait NailMessageHandler {
    /// Create a new Message Handler
    fn new() -> Self;
    /// Handle a message.
    fn handle(&self, chan: Sender<NailMessage>, message: NailMessage);
}

/// A Nail Server Message
pub struct NailMessage {
    token: Token,
    bytes: Vec<u8>,
}

impl NailMessage {
    /// Create a new Nail Message
    pub fn new(token: Token, bytes: Vec<u8>) -> NailMessage {
        NailMessage {
            token: token,
            bytes: bytes,
        }
    }

    /// Get the message bytes
    pub fn bytes(&self) -> &Vec<u8> {
        &self.bytes
    }

    /// Convert the message bytes to a String via `from_utf8_lossy`
    pub fn message(&self) -> String {
        String::from_utf8_lossy(&self.bytes.clone()[..]).into_owned()
    }

    /// Get the token associated with the message.
    pub fn token(&self) -> Token {
        self.token
    }
}

impl fmt::Display for NailMessage {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{{ token: Token({}), message: {} }}",
               self.token.0,
               self.message())

    }
}

/// Server Struct
pub struct Server<T>
    where T: NailMessageHandler
{
    sock: TcpListener,
    token: Token,
    conns: Slab<ServerConnection<T>, Token>,
    _marker: marker::PhantomData<T>,
}

impl<T> Handler for Server<T>
    where T: NailMessageHandler
{
    type Timeout = usize;
    type Message = NailMessage;

    fn ready(&mut self, event_loop: &mut EventLoop<Server<T>>, token: Token, events: EventSet) {
        assert!(token != Token(0), "[BUG]: Received event for Token(0)");

        if events.is_error() {
            stderr!(true, "Error event for Token({})", token.0);
            self.reset_connection(event_loop, token);
            return;
        }

        if events.is_hup() {
            stderr!(true, "Hup event for Token({})", token.0);
            self.reset_connection(event_loop, token);
            return;
        }

        // We never expect a write event for our `Server` token . A write event for any other token
        // should be handed off to that connection.
        if events.is_writable() {
            stdout!(true, "Write event for Token({})", token.0);
            assert!(self.token != token, "Received writable event for Server");

            self.conn_writable(event_loop, token)
                .unwrap_or_else(|e| {
                    stderr!(true, "Write event failed for Token({}), {}", token.0, e);
                    self.reset_connection(event_loop, token);
                });
        }

        // A read event for our `Server` token means we are establishing a new connection. A read
        // event for any other token should be handed off to that connection.
        if events.is_readable() {
            if self.token == token {
                self.accept(event_loop);
            } else {
                self.conn_readable(event_loop, token)
                    .unwrap_or_else(|e| {
                        stderr!(true, "Read event failed for Token({}): {}", token.0, e);
                        self.reset_connection(event_loop, token);
                    });
            }
        }
    }

    fn notify(&mut self, event_loop: &mut EventLoop<Server<T>>, msg: NailMessage) {
        stdout!(true, "msg = {}", msg);

        let msg_str = msg.message();

        if &msg_str == "shutdown" {
            event_loop.shutdown();
        } else if MODE_STREAM.is_match(&msg_str) {
            if let Some(caps) = MODE_STREAM.captures(&msg_str) {
                if let Some(size) = caps.at(1) {
                    stdout!(true, "Starting Streaming Mode");
                    let token = msg.token();
                    let expected: usize = size.parse().unwrap_or(0);
                    stdout!(true, "Expecting {} bytes", expected);
                    if let Ok(conn) = self.get_connection(token) {
                        let timeout = event_loop.timeout(token.0, Duration::from_secs(60));
                        conn.mode(Mode::Stream);
                        conn.expected(expected);
                        conn.timeout(timeout.ok());
                    }
                } else {
                    stderr!(true, "Unable to get first capture from regex");
                }
            } else {
                stderr!(true, "Unable to get captures from regex");
            }
        } else {
            let token = msg.token();
            let msg_bytes = msg.bytes();
            if let Ok(conn) = self.get_connection(token) {
                if let Ok(count) = conn.write(&msg_bytes[..]) {
                    stdout!(true, "Writing {} bytes", count);
                }
                if let Err(e) = event_loop.reregister(&conn.sock,
                                                      token,
                                                      EventSet::writable(),
                                                      PollOpt::edge() | PollOpt::oneshot()) {
                    stderr!(true, "Unable to reregister with event loop: {}", e);
                }
            } else {
                stderr!(true, "Unable to get connection for Token({})", token.0);
            }
        }
    }

    fn timeout(&mut self, _event_loop: &mut EventLoop<Server<T>>, timeout: usize) {
        stdout!(true, "timeout = {}", timeout);
    }

    fn interrupted(&mut self, _event_loop: &mut EventLoop<Server<T>>) {
        stdout!(true, "interrupted");
    }
}

impl<T> Server<T>
    where T: NailMessageHandler
{
    /// Create a new Server
    pub fn new(srv: TcpListener, _marker: T) -> Server<T> {
        Server {
            sock: srv,
            token: Token(1),
            conns: Slab::new_starting_at(Token(2), 128),
            _marker: marker::PhantomData,
        }
    }

    /// Accept a _new_ client connection.
    ///
    /// The server will keep track of the new connection and forward any events from the event loop
    /// to this connection.
    #[cfg_attr(feature="clippy", allow(indexing_slicing))]
    fn accept(&mut self, event_loop: &mut EventLoop<Server<T>>) {
        stdout!(true, "server accepting new socket");

        // Log an error if there is no socket, but otherwise move on so we do not tear down the
        // entire server.
        let (sock, addr) = match self.sock.accept() {
            Ok(s) => {
                if let Some(sock) = s {
                    sock
                } else {
                    stderr!(true, "Failed to accept new socket");
                    self.reregister(event_loop);
                    return;
                }
            }
            Err(e) => {
                stderr!(true, "Failed to accept new socket, {}", e);
                self.reregister(event_loop);
                return;
            }
        };

        if let Some(token) = self.conns.insert_with_opt(|token| {
            let mut new_conn =
                ServerConnection::new(sock, <T as NailMessageHandler>::new());
            new_conn.token(token);
            new_conn.addr(addr);
            match event_loop.register(new_conn.get_sock(),
                                      token,
                                      EventSet::readable(),
                                      PollOpt::edge() | PollOpt::oneshot()) {
                Ok(_) => Some(new_conn),
                Err(_) => None,
            }
        }) {
            stdout!(true,
                    "Assigned Token({}) for connection from {}",
                    token.0,
                    addr);
        } else {
            stderr!(true, "Failed to accept new socket");
            self.reregister(event_loop);
            return;
        }

        // We are using edge-triggered polling. Even our SERVER token needs to reregister.
        self.reregister(event_loop);
    }

    fn conn_readable(&mut self,
                     event_loop: &mut EventLoop<Server<T>>,
                     tok: Token)
                     -> NailResult<()> {
        self.get_connection(tok).and_then(|conn| conn.readable(event_loop))
    }

    fn conn_writable(&mut self,
                     event_loop: &mut EventLoop<Server<T>>,
                     tok: Token)
                     -> NailResult<()> {
        self.get_connection(tok).and_then(|conn| conn.writable(event_loop))
    }

    /// Register listener with the event loop
    pub fn register(&mut self, event_loop: &mut EventLoop<Server<T>>) {
        event_loop.register(&self.sock,
                      self.token,
                      EventSet::readable(),
                      PollOpt::edge() | PollOpt::oneshot())
            .unwrap_or_else(|e| {
                stderr!(true,
                        "Failed to reregister server Token({}): {}",
                        self.token.0,
                        e);
                let server_token = self.token;
                self.reset_connection(event_loop, server_token);
            })
    }

    /// Re-register Server with the event loop.
    ///
    /// This keeps the registration details neatly tucked away inside of our implementation.
    fn reregister(&mut self, event_loop: &mut EventLoop<Server<T>>) {
        event_loop.reregister(&self.sock,
                        self.token,
                        EventSet::readable(),
                        PollOpt::edge() | PollOpt::oneshot())
            .unwrap_or_else(|e| {
                stderr!(true,
                        "Failed to reregister server Token({}): {}",
                        self.token.0,
                        e);
                let server_token = self.token;
                self.reset_connection(event_loop, server_token);
            })
    }

    fn reset_connection(&mut self, event_loop: &mut EventLoop<Server<T>>, token: Token) {
        if self.token == token {
            event_loop.shutdown();
        } else {
            self.conns.remove(token);
        }
    }

    /// Find a connection in the slab using the given token.
    fn get_connection(&mut self, token: Token) -> NailResult<&mut ServerConnection<T>> {
        match self.conns.get_mut(token) {
            Some(conn) => Ok(conn),
            None => Err(NailErr::Token),
        }
    }
}

/// Connection Mode
#[allow(dead_code)]
enum Mode {
    Message,
    Stream,
}

/// `ServerConnection` Struct
struct ServerConnection<T>
    where T: NailMessageHandler
{
    rx: RingBuf,
    tx: RingBuf,
    interest: EventSet,
    mode: Mode,
    sock: TcpStream,
    curr_stream_bytes: usize,
    expected: usize,
    token: Option<Token>,
    timeout: Option<Timeout>,
    addr: Option<SocketAddr>,
    handler: T,
}

impl<T> ServerConnection<T>
    where T: NailMessageHandler
{
    fn new(sock: TcpStream, handler: T) -> ServerConnection<T> {
        ServerConnection {
            rx: RingBuf::new(2048),
            tx: RingBuf::new(2048),
            interest: EventSet::hup(),
            mode: Mode::Message,
            sock: sock,
            curr_stream_bytes: 0,
            expected: 0,
            token: None,
            timeout: None,
            addr: None,
            handler: handler,
        }
    }

    fn get_sock(&self) -> &TcpStream {
        &self.sock
    }

    fn token(&mut self, token: Token) -> &mut ServerConnection<T> {
        self.token = Some(token);
        self
    }

    fn addr(&mut self, addr: SocketAddr) -> &mut ServerConnection<T> {
        self.addr = Some(addr);
        self
    }

    fn expected(&mut self, expected: usize) -> &mut ServerConnection<T> {
        self.expected = expected;
        self
    }

    fn mode(&mut self, mode: Mode) -> &mut ServerConnection<T> {
        self.mode = mode;
        self
    }

    fn timeout(&mut self, timeout: Option<Timeout>) -> &mut ServerConnection<T> {
        self.timeout = timeout;
        self
    }

    fn write(&mut self, bytes: &[u8]) -> NailResult<usize> {
        Ok(self.tx.write_slice(bytes))
    }

    fn writable(&mut self, event_loop: &mut EventLoop<Server<T>>) -> NailResult<()> {
        if let Some(token) = self.token {
            match self.sock.try_write_buf(&mut self.tx) {
                Ok(None) => {
                    stdout!(true, "client flushing buf; WOULDBLOCK");
                    self.interest.insert(EventSet::writable());
                }
                Ok(Some(r)) => {
                    stdout!(true, "CONN : we wrote {} bytes!", r);
                    self.interest.insert(EventSet::readable());
                    self.interest.remove(EventSet::writable());
                }
                Err(e) => stderr!(true, "not implemented; client err={}", e),
            }

            Ok(try!(event_loop.reregister(&self.sock,
                                          token,
                                          self.interest,
                                          PollOpt::edge() | PollOpt::oneshot())))
        } else {
            Err(NailErr::Token)
        }
    }

    fn readable(&mut self, event_loop: &mut EventLoop<Server<T>>) -> NailResult<()> {
        if let (Some(_), Some(token)) = (self.addr, self.token) {
            match self.sock.try_read_buf(&mut self.rx) {
                Ok(None) => {}
                Ok(Some(r)) => {
                    match self.mode {
                        Mode::Message => {
                            let mut bytes_read = Vec::with_capacity(r);
                            bytes_read.extend_from_slice(self.rx.bytes());
                            let nail_message = NailMessage::new(token, bytes_read);
                            let channel = event_loop.channel();
                            self.handler.handle(channel, nail_message);
                        }
                        Mode::Stream => {
                            self.curr_stream_bytes += r;
                            stdout!(true, "{}/{}", self.curr_stream_bytes, self.expected);
                        }
                    }

                    Buf::advance(&mut self.rx, r);
                    self.interest.insert(EventSet::readable());
                }
                Err(e) => {
                    stderr!(true, "not implemented; client err={}", e);
                    self.interest.remove(EventSet::readable());
                }
            };

            Ok(try!(event_loop.reregister(&self.sock,
                                          token,
                                          self.interest,
                                          PollOpt::edge() | PollOpt::oneshot())))
        } else {
            Err(NailErr::Token)
        }
    }
}

#[cfg(test)]
mod test {
    use mio::{EventLoop, Ipv4Addr, Sender};
    use mio::tcp::{TcpListener, TcpStream};
    use rand::{self, Rng};
    use std::io::{BufWriter, Write};
    use std::net::{SocketAddr, SocketAddrV4};
    use std::sync::mpsc::channel;
    use std::thread;
    use std::time::Duration;
    use super::{NailMessage, NailMessageHandler, Server};

    struct MyHandler;

    impl NailMessageHandler for MyHandler {
        fn new() -> MyHandler {
            MyHandler
        }

        fn handle(&self, channel: Sender<NailMessage>, message: NailMessage) {
            thread::spawn(move || {
                stdout!(true, "Read '{}'", message);
                let mut rng = rand::thread_rng();
                let n = rng.gen_range(0, 10);
                thread::sleep(Duration::from_secs(n));
                channel.send(message).expect("Unable to notify event loop");
            });
        }
    }

    #[test]
    fn tcp_server() {
        // Setup the eventloop.
        let mut eloop = EventLoop::new().expect("Unable to create eventloop");
        stdout!(true, "created eventloop");

        // Setup the listener socket
        let ip = Ipv4Addr::new(0, 0, 0, 0);
        let addr = SocketAddr::V4(SocketAddrV4::new(ip, 12345));
        let listener = TcpListener::bind(&addr).expect("Unable to bind tcp listener");

        // Setup a sync channel
        let (tx, rx) = channel();

        // Setup a tcp stream for sending messages.
        let mut sender = BufWriter::new(TcpStream::connect(&addr)
            .expect("Unable to connect to listener"));

        // Spawn a thread to send a couple broadcast messages and shutdown the eventloop.
        thread::spawn(move || {
            // I've seen things you people wouldn't believe. Attack ships on fire off the
            // shoulder
            // of Orion. I watched C-beams glitter in the dark near the Tannhauser Gate. All
            // those
            // moments will be lost in time, like tears...in...rain.
            sender.write_all(b"Time to die").expect("Failed to write to listener");
            sender.flush().expect("unable to flush buffer");
            thread::sleep(Duration::from_secs(5));
            sender.write_all(b"shutdown").expect("failed to send shutdown");
            sender.flush().expect("unable to flush buffer");

            thread::sleep(Duration::from_secs(10));
            tx.send(0).expect("Unable to send 0");
        });

        // Setup the handler
        let mut handler = Server::new(listener, MyHandler);
        // Register the handler with the event loop.
        handler.register(&mut eloop);
        // Run the event loop.
        eloop.run(&mut handler).expect("Unable to run event loop");
        rx.recv().expect("Nothing received");
    }
}
