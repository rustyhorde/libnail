use ::NailResult;
use bytes::{Buf, MutBuf, RingBuf};
use error::NailErr;
use mio::{EventSet, EventLoop, Handler, PollOpt, Sender, Token, TryRead, TryWrite};
use mio::tcp::{TcpListener, TcpStream};
use ssh::{CipherBlockSize, KeyExchangeInit, MacAlgorithm, SshPacket, SshPayload, VER_EXCH_PROTO,
          VERSION};
use slab::Slab;
use ssh::VersionExchange;
use std::fmt;
use std::marker::PhantomData;

#[derive(Clone, Copy)]
enum ConnectionState {
    VersionExchange,
    KeyExchangeInit,
    KeyExchange,
    Secured,
}

impl fmt::Display for ConnectionState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::ConnectionState::*;

        let cs_str = match *self {
            VersionExchange => "VersionExchange",
            KeyExchangeInit => "KeyExchangeInit",
            KeyExchange => "KeyExchange",
            Secured => "Secured",
        };
        write!(f, "{}", cs_str)
    }
}

enum ConnectionEvent {
    ClientVersionExchangeReceived,
    ClientKeyExchangeReceived,
    #[allow(dead_code)]
    NewKeys,
}

impl ConnectionState {
    pub fn next(self, event: ConnectionEvent) -> ConnectionState {
        use self::ConnectionEvent::*;
        use self::ConnectionState::*;

        match (self, event) {
            (VersionExchange, ClientVersionExchangeReceived) => KeyExchangeInit,
            (KeyExchangeInit, ClientKeyExchangeReceived) => KeyExchange,
            (KeyExchange, NewKeys) => Secured,
            (_, _) => self,
        }
    }
}

/// Handler Trait for `SshPacket`
pub trait SshPacketHandler {
    /// Create a new SshPacketHandler
    fn new() -> Self;
    /// Handle an SshPacket. Use the channel to send a response if necessary.
    fn handle_packet(&self, channel: Sender<SshPacket>, payload: SshPayload);
}

/// An SSH-2 Daemon
pub struct SshDaemon<T>
    where T: SshPacketHandler
{
    sock: TcpListener,
    token: Token,
    conns: Slab<SshdConnection<T>, Token>,
    ves: String,
    _marker: PhantomData<T>,
}

impl<T> Handler for SshDaemon<T>
    where T: SshPacketHandler
{
    type Timeout = usize;
    type Message = SshPacket;

    fn ready(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>, token: Token, events: EventSet) {
        assert!(token != Token(0), "[BUG]: Received event for Token(0)");

        if events.is_hup() {
            stderr!(true, "Hup event for Token({})", token.0);
            self.reset_connection(event_loop, token);
            return;
        }

        if events.is_error() {
            stderr!(true, "Error event for Token({})", token.0);
            self.reset_connection(event_loop, token);
            return;
        }

        // A read event for our `SshDaemon` token means we are establishing a new connection. A read
        // event for any other token should be handed off to that connection.
        if events.is_readable() {
            if self.token == token {
                self.accept(event_loop);
            } else {
                self.conn_readable(event_loop, token)
                    .unwrap_or_else(|e| {
                        stderr!(true, "Read event failed for Token({}): {}", token.0, e);
                        self.reset_connection(event_loop, token);
                    });
            }
        }

        // We never expect a write event for our `SshDaemon` token . A write event for any other
        // token should be handed off to that connection.
        if events.is_writable() {
            stdout!(true, "Write event for Token({})", token.0);
            assert!(self.token != token, "Received writable event for SshDaemon");

            self.conn_writable(event_loop, token)
                .unwrap_or_else(|e| {
                    stderr!(true, "Write event failed for Token({}), {}", token.0, e);
                    self.reset_connection(event_loop, token);
                });
        }
    }

    fn timeout(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>, timeout: usize) {
        assert_eq!(timeout, 123);
        event_loop.shutdown();
    }
}

impl<T> SshDaemon<T>
    where T: SshPacketHandler
{
    /// Create a new SshDaemon
    pub fn new(srv: TcpListener, _marker: T) -> SshDaemon<T> {
        let mut ver_str = String::new();
        ver_str.push_str("SSH-2.0-libnail_");
        ver_str.push_str(VERSION.unwrap_or("unk"));
        ver_str.push_str("\r\n");
        SshDaemon {
            sock: srv,
            token: Token(1),
            conns: Slab::new_starting_at(Token(2), 128),
            ves: ver_str,
            _marker: PhantomData,
        }
    }

    /// Accept a _new_ client connection.
    ///
    /// The server will keep track of the new connection and forward any events from the event loop
    /// to this connection.
    #[cfg_attr(feature="clippy", allow(indexing_slicing))]
    fn accept(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>) {
        stdout!(true, "server accepting new socket");

        // Log an error if there is no socket, but otherwise move on so we do not tear down the
        // entire server.
        let (sock, addr) = match self.sock.accept() {
            Ok(s) => {
                if let Some(sock) = s {
                    sock
                } else {
                    stderr!(true, "Failed to accept new socket");
                    self.reregister(event_loop);
                    return;
                }
            }
            Err(e) => {
                stderr!(true, "Failed to accept new socket, {}", e);
                self.reregister(event_loop);
                return;
            }
        };

        let ves = self.ves.clone();

        if let Some(token) = self.conns.insert_with_opt(|token| {
            let mut version_exchange: VersionExchange = Default::default();
            version_exchange.set_server_version(ves);
            let new_conn =
                SshdConnection::new(sock,
                                    token,
                                    version_exchange,
                                    <T as SshPacketHandler>::new());
            match event_loop.register(new_conn.sock(),
                                      token,
                                      EventSet::readable(),
                                      PollOpt::edge() | PollOpt::oneshot()) {
                Ok(_) => Some(new_conn),
                Err(_) => None,
            }
        }) {
            stdout!(true,
                    "Assigned Token({}) for connection from {}",
                    token.0,
                    addr);
            stdout!(true, "Sending version string: {}", self.ves);
            let ves_bytes = self.ves.bytes().collect::<Vec<u8>>();
            if let Ok(conn) = self.get_connection(token) {
                if let Ok(count) = conn.write(event_loop, &ves_bytes) {
                    stdout!(true, "Writing {} bytes", count);
                }
            } else {
                stderr!(true, "Unable to get connection for Token({})", token.0);
            }
        } else {
            stderr!(true, "Failed to accept new socket");
            self.reregister(event_loop);
            return;
        }

        // We are using edge-triggered polling. Even our SERVER token needs to reregister.
        self.reregister(event_loop);
    }

    /// Get the connection for the given token and call readable on that connection.
    fn conn_readable(&mut self,
                     event_loop: &mut EventLoop<SshDaemon<T>>,
                     tok: Token)
                     -> NailResult<()> {
        self.get_connection(tok).and_then(|conn| conn.readable(event_loop))
    }

    /// Get the connection for the given token and call writable on that connection.
    fn conn_writable(&mut self,
                     event_loop: &mut EventLoop<SshDaemon<T>>,
                     tok: Token)
                     -> NailResult<()> {
        self.get_connection(tok).and_then(|conn| conn.writable(event_loop))
    }

    /// Find a connection in the slab using the given token.
    fn get_connection(&mut self, token: Token) -> NailResult<&mut SshdConnection<T>> {
        match self.conns.get_mut(token) {
            Some(conn) => Ok(conn),
            None => Err(NailErr::Token),
        }
    }

    /// Register listener with the event loop
    pub fn register(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>) {
        event_loop.register(&self.sock,
                      self.token,
                      EventSet::readable(),
                      PollOpt::edge() | PollOpt::oneshot())
            .unwrap_or_else(|e| {
                stderr!(true,
                        "Failed to reregister server Token({}): {}",
                        self.token.0,
                        e);
                let server_token = self.token;
                self.reset_connection(event_loop, server_token);
            })
    }

    /// Re-register `SshDaemon` with the event loop.
    fn reregister(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>) {
        event_loop.reregister(&self.sock,
                        self.token,
                        EventSet::readable(),
                        PollOpt::edge() | PollOpt::oneshot())
            .unwrap_or_else(|e| {
                stderr!(true,
                        "Failed to reregister server Token({}): {}",
                        self.token.0,
                        e);
                let server_token = self.token;
                self.reset_connection(event_loop, server_token);
            })
    }

    fn reset_connection(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>, token: Token) {
        if self.token == token {
            event_loop.shutdown();
        } else {
            self.conns.remove(token);
        }
    }
}

/// `ServerConnection` Struct
struct SshdConnection<T>
    where T: SshPacketHandler
{
    state: ConnectionState,
    rx: RingBuf,
    tx: RingBuf,
    interest: EventSet,
    sock: TcpStream,
    token: Token,
    handler: T,
    version_exchange: VersionExchange,
}

impl<T> SshdConnection<T>
    where T: SshPacketHandler
{
    fn new(sock: TcpStream,
           token: Token,
           version_exchange: VersionExchange,
           handler: T)
           -> SshdConnection<T> {
        SshdConnection {
            state: ConnectionState::VersionExchange,
            handler: handler,
            interest: EventSet::hup(),
            rx: RingBuf::new(2048),
            sock: sock,
            token: token,
            tx: RingBuf::new(2048),
            version_exchange: version_exchange,
        }
    }

    #[cfg_attr(feature="clippy", allow(use_debug))]
    fn readable(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>) -> NailResult<()> {
        match self.sock.try_read_buf(&mut self.rx) {
            Ok(None) => {}
            Ok(Some(r)) => {
                stdout!(true, "Read {} bytes", r);
                let mut bytes_read = Vec::with_capacity(r);
                bytes_read.extend_from_slice(self.rx.bytes());
                match self.state {
                    ConnectionState::VersionExchange => {
                        try!(self.version_exchange(&bytes_read, r));
                        let kex_init: KeyExchangeInit = Default::default();
                        let payload = try!(kex_init.into_bytes());
                        let ssh_packet: SshPacket = Default::default();
                        let packet = try!(ssh_packet.as_bytes(payload,
                                                              CipherBlockSize::Eight,
                                                              MacAlgorithm::NoMac));
                        try!(self.write(event_loop, &packet));
                    }
                    ConnectionState::KeyExchangeInit => {
                        let ssh_packet: SshPacket = Default::default();
                        let payload = try!(ssh_packet.into_payload(bytes_read));

                        if let SshPayload::KeyExchangeInit(kex) = payload {
                            stdout!(true, "Kex: {:?}", kex);
                            self.state = self.state
                                .next(ConnectionEvent::ClientKeyExchangeReceived);
                        } else {
                            return Err(NailErr::Token);
                        }
                    }
                    ConnectionState::KeyExchange => {
                        let ssh_packet: SshPacket = Default::default();
                        if let Ok(payload) = ssh_packet.into_payload(bytes_read) {
                            let channel = event_loop.channel();
                            self.handler.handle_packet(channel, payload);
                        } else {
                            stderr!(true, "Invalid Packet Received");
                        }
                    }
                    _ => {
                        stderr!(true, "Invalid Readable State: {}", self.state);
                        return Err(NailErr::Token);
                    }
                }

                Buf::advance(&mut self.rx, r);
                self.interest.insert(EventSet::readable());
            }
            Err(e) => {
                stderr!(true, "not implemented; client err={}", e);
                self.interest.remove(EventSet::readable());
            }
        };

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    fn sock(&self) -> &TcpStream {
        &self.sock
    }

    fn writable(&mut self, event_loop: &mut EventLoop<SshDaemon<T>>) -> NailResult<()> {
        match self.sock.try_write_buf(&mut self.tx) {
            Ok(None) => {
                stdout!(true, "client flushing buf; WOULDBLOCK");
                self.interest.insert(EventSet::writable());
            }
            Ok(Some(r)) => {
                stdout!(true, "CONN : we wrote {} bytes!", r);
                self.interest.insert(EventSet::readable());
                self.interest.remove(EventSet::writable());
            }
            Err(e) => stderr!(true, "not implemented; client err={}", e),
        }

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    fn write(&mut self,
             event_loop: &mut EventLoop<SshDaemon<T>>,
             bytes: &[u8])
             -> NailResult<usize> {
        let count = self.tx.write_slice(bytes);
        try!(event_loop.reregister(&self.sock,
                                   self.token,
                                   EventSet::writable(),
                                   PollOpt::edge() | PollOpt::oneshot()));
        Ok(count)
    }

    fn version_exchange(&mut self, bytes: &[u8], r: usize) -> NailResult<()> {
        // Parse any message here as a protocol version exchange message.  See
        // https://tools.ietf.org/html/rfc4253#section-4.2.
        // If the message is longer that 255 bytes or fails to match the message
        // format shutdown the connection
        let vep = String::from_utf8_lossy(bytes);
        if r < 256 && VER_EXCH_PROTO.is_match(&vep) {
            self.version_exchange.set_client_version(vep.into_owned());
            self.state = self.state.next(ConnectionEvent::ClientVersionExchangeReceived);
            stdout!(true, "{}", self.version_exchange);
            Ok(())
        } else {
            stderr!(true, "Invalid Version Exchange: {}", vep);
            Err(NailErr::Token)
        }
    }
}

#[cfg(test)]
mod test {
    use mio::{EventLoop, Ipv4Addr, Sender};
    use mio::tcp::{TcpListener, TcpStream};
    use rand::{self, Rng};
    use ssh::{CipherBlockSize, KeyExchangeInit, MacAlgorithm, SshPacket, SshPayload};
    use std::io::{BufReader, BufWriter, Read, Write};
    use std::net::{Shutdown, SocketAddr, SocketAddrV4};
    use std::sync::mpsc::channel;
    use std::thread;
    use std::time::Duration;
    use super::{SshPacketHandler, SshDaemon};

    struct MyHandler;

    impl SshPacketHandler for MyHandler {
        fn new() -> MyHandler {
            MyHandler
        }

        fn handle_packet(&self, _channel: Sender<SshPacket>, _packet: SshPayload) {
            thread::spawn(move || {
                let mut rng = rand::thread_rng();
                let n = rng.gen_range(0, 10);
                thread::sleep(Duration::from_secs(n));
                // channel.send(packet).expect("Unable to notify event loop");
            });
        }
    }

    #[test]
    #[ignore]
    #[cfg_attr(feature = "clippy", allow(indexing_slicing))]
    fn ssh_daemon() {
        // Setup the eventloop.
        let mut eloop = EventLoop::new().expect("Unable to create eventloop");
        stdout!(true, "created eventloop");

        // Setup the listener socket
        let ip = Ipv4Addr::new(0, 0, 0, 0);
        let addr = SocketAddr::V4(SocketAddrV4::new(ip, 2222));
        let listener = TcpListener::bind(&addr).expect("Unable to bind tcp listener");

        // Setup a sync channel
        let (tx, rx) = channel();

        // Setup a tcp stream for sending messages.
        let stream = TcpStream::connect(&addr).expect("Unable to connect to listener");
        let read_stream = stream.try_clone().expect("Unable to clone tcpstream");
        let mut swtr = BufWriter::new(stream);
        let mut srdr = BufReader::new(read_stream);

        thread::spawn(move || {
            let mut buffer = [0u8; 2048];
            loop {
                if let Ok(c) = srdr.read(&mut buffer) {
                    if c == 0 {
                        break;
                    } else {
                        stdout!(true, "{}", String::from_utf8_lossy(&buffer[0..c]));
                    }
                }
            }
        });

        // Spawn a thread to send the Version Exchange string.
        thread::spawn(move || {
            // Send a Version Exchange string.
            swtr.write_all(b"SSH-2.0-testclient_1.0 commenting1234\r\n")
                .expect("Failed to write version exchange");
            swtr.flush().expect("unable to flush buffer");
            thread::sleep(Duration::from_secs(5));
            let kex: KeyExchangeInit = Default::default();
            if let Ok(kex_bytes) = kex.into_bytes() {
                let ssh_packet: SshPacket = Default::default();
                if let Ok(sshb) =
                       ssh_packet.as_bytes(kex_bytes, CipherBlockSize::Eight, MacAlgorithm::NoMac) {
                    swtr.write_all(&sshb).expect("Unable to write kex packet");
                    swtr.flush().expect("unable to flush buffer");
                } else {
                    assert!(false);
                }
            } else {
                assert!(false);
            }
            thread::sleep(Duration::from_secs(5));
            let stream = swtr.get_ref();
            stream.shutdown(Shutdown::Both).expect("Unable to shutdown tcpstream");
            tx.send(0).expect("Unable to send 0");
        });

        // Setup the handler
        let mut handler = SshDaemon::new(listener, MyHandler);
        // Register the handler with the event loop.
        handler.register(&mut eloop);
        // Setup a timeout to shutdown the loop
        eloop.timeout(123, Duration::from_secs(15)).expect("Unable to set shutdown timeout");
        // Run the event loop.
        eloop.run(&mut handler).expect("Unable to run event loop");

        rx.recv().expect("Nothing received");
    }
}
