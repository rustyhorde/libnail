use ::NailResult;
use bytes::{Buf, MutBuf, RingBuf};
use mio::{EventLoop, EventSet, Handler, PollOpt, Token, TryRead, TryWrite};
use mio::tcp::TcpStream;
use ::server::NailMessage;

pub trait MessageHandler {
    fn new() -> Self;
    fn handle(&self, message: NailMessage);
}

/// Client Struct
pub struct Client<T>
    where T: MessageHandler
{
    sock: TcpStream,
    token: Token,
    rx: RingBuf,
    tx: RingBuf,
    interest: EventSet,
    handler: T,
}

impl<T> Client<T>
    where T: MessageHandler
{
    /// Create a new Client on the given socket
    pub fn new(sock: TcpStream, handler: T) -> Client<T> {
        Client {
            sock: sock,
            token: Token(1),
            rx: RingBuf::new(2048),
            tx: RingBuf::new(2048),
            interest: EventSet::hup(),
            handler: handler,
        }
    }

    /// Register listener with the event loop
    pub fn register(&mut self, event_loop: &mut EventLoop<Client<T>>) {
        event_loop.register(&self.sock,
                      self.token,
                      EventSet::readable(),
                      PollOpt::edge() | PollOpt::oneshot())
            .unwrap_or_else(|e| {
                stderr!(true,
                        "Failed to register client Token({}): {}",
                        self.token.0,
                        e);
                event_loop.shutdown();
            })
    }

    fn send(&mut self, event_loop: &mut EventLoop<Client<T>>, bytes: &[u8]) -> NailResult<usize> {
        let count = self.tx.write_slice(bytes);
        self.interest.insert(EventSet::readable());
        self.interest.insert(EventSet::writable());
        try!(event_loop.reregister(&self.sock,
                                   self.token,
                                   self.interest,
                                   PollOpt::edge() | PollOpt::oneshot()));
        Ok(count)
    }

    fn writable(&mut self, event_loop: &mut EventLoop<Client<T>>) -> NailResult<()> {
        match self.sock.try_write_buf(&mut self.tx) {
            Ok(None) => {
                self.interest.insert(EventSet::writable());
            }
            Ok(Some(r)) => {
                stdout!(true, "CONN : we wrote {} bytes!", r);
                self.interest.insert(EventSet::readable());
                self.interest.remove(EventSet::writable());
            }
            Err(e) => stderr!(true, "not implemented; client err={}", e),
        }

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    fn readable(&mut self, event_loop: &mut EventLoop<Client<T>>) -> NailResult<()> {
        match self.sock.try_read_buf(&mut self.rx) {
            Ok(None) => {}
            Ok(Some(r)) => {
                let mut bytes_read = Vec::with_capacity(r);
                bytes_read.extend_from_slice(self.rx.bytes());
                let nail_message = NailMessage::new(self.token, bytes_read);
                // let channel = event_loop.channel();
                self.handler.handle(nail_message);
                Buf::advance(&mut self.rx, r);
                self.interest.insert(EventSet::readable());
            }
            Err(e) => {
                stderr!(true, "Unable to read from socket: {}", e);
                self.interest.remove(EventSet::readable());
            }
        };

        Ok(try!(event_loop.reregister(&self.sock,
                                      self.token,
                                      self.interest,
                                      PollOpt::edge() | PollOpt::oneshot())))
    }

    fn reset_connection(&mut self, event_loop: &mut EventLoop<Client<T>>) {
        event_loop.shutdown();
    }
}

impl<T> Handler for Client<T>
    where T: MessageHandler
{
    type Timeout = usize;
    type Message = String;

    fn ready(&mut self, event_loop: &mut EventLoop<Client<T>>, token: Token, events: EventSet) {
        assert!(token != Token(0), "[BUG]: Received event for Token(0)");

        if events.is_error() {
            stderr!(true, "Error event for Token({})", token.0);
            self.reset_connection(event_loop);
            return;
        }

        if events.is_hup() {
            stderr!(true, "Hup event for Token({})", token.0);
            self.reset_connection(event_loop);
            return;
        }

        if events.is_readable() {
            self.readable(event_loop).unwrap_or_else(|e| {
                stderr!(true, "Error in readable: {}", e);
                self.reset_connection(event_loop);
            });
        }

        if events.is_writable() {
            self.writable(event_loop).unwrap_or_else(|e| {
                stderr!(true, "Error in writable: {}", e);
                self.reset_connection(event_loop);
            });
        }
    }

    fn notify(&mut self, event_loop: &mut EventLoop<Client<T>>, msg: String) {
        stdout!(true, "msg = {}", msg);

        // if &msg == "shutdown" {
        //     event_loop.shutdown();
        // } else {
        if let Ok(count) = self.send(event_loop, &msg.into_bytes()) {
            stdout!(true, "Writing {} bytes", count);
        }
        if let Err(e) = event_loop.reregister(&self.sock,
                                              self.token,
                                              EventSet::writable(),
                                              PollOpt::edge() | PollOpt::oneshot()) {
            stderr!(true, "Unable to reregister with event loop: {}", e);
        }
        // }
    }
}

#[cfg(test)]
mod test {
    use mio::{EventLoop, Ipv4Addr, Sender};
    use mio::tcp::{TcpListener, TcpStream};
    use rand::{self, Rng};
    use std::net::{SocketAddr, SocketAddrV4};
    use std::sync::mpsc::channel;
    use std::thread;
    use std::time::Duration;
    use server::{NailMessage, NailMessageHandler, Server};
    use super::{Client, MessageHandler};

    struct ServerMessageHandler;

    impl NailMessageHandler for ServerMessageHandler {
        fn new() -> ServerMessageHandler {
            ServerMessageHandler
        }

        fn handle(&self, channel: Sender<NailMessage>, message: NailMessage) {
            thread::spawn(move || {
                stdout!(true, "Read '{}'", message);
                let mut rng = rand::thread_rng();
                let n = rng.gen_range(0, 10);
                thread::sleep(Duration::from_secs(n));
                channel.send(message).expect("Unable to notify event loop");
            });
        }
    }

    struct MyMessageHandler;

    impl MessageHandler for MyMessageHandler {
        fn new() -> MyMessageHandler {
            MyMessageHandler
        }

        fn handle(&self, message: NailMessage) {
            stdout!(true, "Received '{}' from server", message.message());
        }
    }

    #[test]
    fn tcp_client_server() {
        // Setup the server eventloop.
        let mut eloop = EventLoop::new().expect("Unable to create eventloop");
        stdout!(true, "created eventloop");

        // Setup the server listener socket
        let ip = Ipv4Addr::new(0, 0, 0, 0);
        let addr = SocketAddr::V4(SocketAddrV4::new(ip, 12346));
        let listener = TcpListener::bind(&addr).expect("Unable to bind tcp listener");

        // Setup a sync channel
        let (tx, rx) = channel();

        // Setup the handler
        let mut handler = Server::new(listener, ServerMessageHandler);
        // Register the handler with the event loop.
        handler.register(&mut eloop);

        thread::spawn(move || {
            // Run the server eventloop.
            eloop.run(&mut handler).expect("Unable to run event loop");
            tx.send(0).expect("Unable to close server thread!");
        });

        // Setup the client eventloop.
        let mut client_eloop = EventLoop::new().expect("Unable to create client eventloop");
        let client_channel = client_eloop.channel();
        let client_stream = TcpStream::connect(&addr).expect("Unable to connect to listener");
        let mut client_handler = Client::new(client_stream, MyMessageHandler::new());
        client_handler.register(&mut client_eloop);

        // Setup the client sync channel
        let (tx1, rx1) = channel();

        // Spawn a thread with the client event loop.
        thread::spawn(move || {
            client_eloop.run(&mut client_handler).expect("Unable to run client eventloop");
            tx1.send(0).expect("Unable to close client thread!");
        });

        client_channel.send("Testing".to_owned()).expect("Unable to send message");
        thread::sleep(Duration::from_secs(10));
        client_channel.send("shutdown".to_owned()).expect("Unable to send shutdown");

        rx.recv().expect("Nothing received");
        rx1.recv().expect("Nothing received");
    }
}
