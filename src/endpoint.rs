//! UDP Endpoint
use ::NailResult;
use bytes::{Buf, MutBuf, RingBuf};
use error::NailErr;
use mio::{EventLoop, EventSet, Handler, PollOpt, Sender, Token};
use mio::udp::UdpSocket;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::fmt;
use std::net::SocketAddr;
use uuid::Uuid;

pub trait MessageHandler {
    fn handle(&self, chan: Sender<Message>, uuid: Uuid, message: String, addr: SocketAddr);
}

/// `Udp`
pub struct Udp<T>
    where T: MessageHandler
{
    sock: UdpSocket,
    token: Token,
    rx: RingBuf,
    tx: RingBuf,
    next: usize,
    addrs: HashMap<SocketAddr, Token>,
    handler: T,
}

impl<T> Udp<T>
    where T: MessageHandler
{
    /// Create a new NailReceiver on the given `UDPSocket`.
    pub fn new(sock: UdpSocket, handler: T) -> Udp<T> {
        Udp {
            sock: sock,
            token: Token(1),
            rx: RingBuf::new(2048),
            tx: RingBuf::new(2048),
            next: 2,
            addrs: HashMap::new(),
            handler: handler,
        }
    }

    /// Register Server with the event loop.
    ///
    /// This keeps the registration details neatly tucked away inside of our implementation.
    pub fn register(&mut self, event_loop: &mut EventLoop<Udp<T>>) -> NailResult<()> {
        event_loop.register(&self.sock,
                      self.token,
                      EventSet::readable(),
                      PollOpt::edge() | PollOpt::oneshot())
            .or_else(|e| Err(NailErr::Io(e)))
    }

    /// Register Server with the event loop.
    ///
    /// This keeps the registration details neatly tucked away inside of our implementation.
    pub fn reregister(&mut self, event_loop: &mut EventLoop<Udp<T>>) {
        event_loop.reregister(&self.sock,
                        self.token,
                        EventSet::readable(),
                        PollOpt::edge() | PollOpt::oneshot())
            .unwrap_or_else(|_| {
                // error!("Failed to reregister server {:?}, {:?}", self.token, e);
                let server_token = self.token;
                self.reset_connection(event_loop, server_token);
            })
    }

    fn reset_connection(&mut self, event_loop: &mut EventLoop<Udp<T>>, token: Token) {
        if self.token == token {
            event_loop.shutdown();
        }
    }
}

impl<T> Handler for Udp<T>
    where T: MessageHandler
{
    type Timeout = usize;
    type Message = Message;

    #[cfg_attr(feature="clippy", allow(use_debug))]
    fn ready(&mut self, event_loop: &mut EventLoop<Udp<T>>, token: Token, events: EventSet) {
        stdout!(true, "events = {:?}", events);

        if events.is_error() {
            stderr!(true, "error event for {:?}", token);
            self.reset_connection(event_loop, token);
            return;
        }

        if events.is_hup() {
            stderr!(true, "hup event for {:?}", token);
            self.reset_connection(event_loop, token);
            return;
        }

        if events.is_readable() {
            stdout!(true, "readable event for {:?}", token);
            let (cnt, addr) = unsafe {
                let (cnt, addr) = match self.sock.recv_from(self.rx.mut_bytes()) {
                    Ok(Some(v)) => v,
                    Ok(None) => {
                        stderr!(true, "recv_from read 0 bytes");
                        return;
                    }
                    Err(e) => {
                        stderr!(true, "recv_from error: {}", e);
                        return;
                    }
                };
                MutBuf::advance(&mut self.rx, cnt);
                (cnt, addr)
            };

            let mut msg_bytes = Vec::with_capacity(cnt);
            msg_bytes.extend_from_slice(self.rx.bytes());
            // let boxed_msg = Box::new(message);
            let uuid = Uuid::new_v4();
            let chan = event_loop.channel();

            match self.addrs.entry(addr) {
                Entry::Occupied(_) => {}
                Entry::Vacant(entry) => {
                    let tok = Token(self.next);
                    self.next += 1;
                    entry.insert(tok);
                }
            }

            let message = String::from_utf8_lossy(&msg_bytes);
            self.handler.handle(chan, uuid, message.into_owned(), addr);

            Buf::advance(&mut self.rx, cnt);
        }

        if events.is_writable() {
            for (addr, tok) in &self.addrs {
                if tok.0 == token.0 {
                    let cnt = Buf::remaining(&self.tx);
                    stdout!(true, "Writable for Token {}", tok.0);
                    stdout!(true, "Bytes: {}", cnt);
                    let mut message = Vec::with_capacity(Buf::remaining(&self.tx));
                    message.extend_from_slice(self.tx.bytes());
                    Buf::advance(&mut self.tx, cnt);
                    stdout!(true, "Writing '{:?}' to {}", message, addr);
                    self.sock.send_to(&message, addr).expect("Didn't send");
                    break;
                }
            }
        }

        self.reregister(event_loop);
    }

    fn notify(&mut self, event_loop: &mut EventLoop<Udp<T>>, msg: Message) {
        stdout!(true, "msg = {}", msg);

        if let "shutdown" = &msg.message()[..] {
            event_loop.shutdown();
        } else if let Some(tok) = self.addrs.get(msg.addr()) {
            let mut bytes = Vec::new();
            bytes.extend(msg.message().bytes());
            self.tx.write_slice(&bytes);
            match event_loop.reregister(&self.sock,
                                        *tok,
                                        EventSet::writable(),
                                        PollOpt::edge() | PollOpt::oneshot()) {
                Ok(_) => {}
                Err(e) => {
                    stderr!(true, "{}", e);
                }
            }
        }
    }

    fn timeout(&mut self, _event_loop: &mut EventLoop<Udp<T>>, timeout: usize) {
        stdout!(true, "timeout = {}", timeout);
    }

    fn interrupted(&mut self, _event_loop: &mut EventLoop<Udp<T>>) {
        stdout!(true, "interrupted");
    }
}

pub struct Message {
    uuid: Uuid,
    addr: SocketAddr,
    message: String,
}

impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "{{ uuid: {}, addr: {}, message: {} }}",
               self.uuid,
               self.addr,
               self.message)
    }
}

impl Message {
    pub fn new(uuid: Uuid, addr: SocketAddr, message: String) -> Message {
        Message {
            uuid: uuid,
            addr: addr,
            message: message,
        }
    }

    fn addr(&self) -> &SocketAddr {
        &self.addr
    }

    fn message(&self) -> &String {
        &self.message
    }
}
