use std::fmt;
use std::io::{self, Write};

pub fn write_stdout(newline: bool, args: fmt::Arguments) {
    if newline {
        writeln!(io::stdout(), "{}", args).expect("Unable to write to stdout!");
    } else {
        write!(io::stdout(), "{}", args).expect("Unable to write to stdout!");
    }
    io::stdout().flush().expect("");
}

pub fn write_stderr(newline: bool, args: fmt::Arguments) {
    if newline {
        writeln!(io::stderr(), "{}", args).expect("Unable to write to stdout!");
    } else {
        write!(io::stderr(), "{}", args).expect("Unable to write to stdout!");
    }
    io::stderr().flush().expect("");
}

#[module_export]
macro_rules! stdout {
    ($newline: expr, $($args_to_format: tt)+) => {
        ::utils::write_stdout($newline, format_args!($($args_to_format)*));
    }
}

#[module_export]
macro_rules! stderr {
    ($newline: expr, $($args_to_format: tt)+) => {
        ::utils::write_stderr($newline, format_args!($($args_to_format)*));
    }
}
