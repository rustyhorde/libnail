//! Nailgun Receiver
//! Used for long running processes
use endpoint::{Message, MessageHandler};
use mio::Sender;
use rand::{self, Rng};
use std::net::SocketAddr;
use std::thread;
use std::time::Duration;
use uuid::Uuid;

/// Stuff
pub struct Receiver;

impl MessageHandler for Receiver {
    fn handle(&self, chan: Sender<Message>, uuid: Uuid, message: String, addr: SocketAddr) {
        thread::spawn(move || {
            stdout!(true, "Moving message: {}", uuid);
            stdout!(true, "Read '{}'", message);
            let mut rng = rand::thread_rng();
            let n = rng.gen_range(0, 10);
            thread::sleep(Duration::from_secs(n));
            stdout!(true, "Processed: {}", uuid);
            chan.send(Message::new(uuid, addr, message))
                .expect("Unable to send shutdown");
        });
    }
}

#[cfg(test)]
mod test {
    use endpoint::Udp;
    use mio::{EventLoop, Ipv4Addr};
    use mio::udp::UdpSocket;
    use std::net::{SocketAddr, SocketAddrV4};
    use std::sync::mpsc::channel;
    use std::time::Duration;
    use std::thread;
    use super::Receiver;

    #[test]
    fn udp_receiver() {
        // Setup the eventloop and notification channel.
        let mut eloop = EventLoop::new().expect("Unable to create eventloop");
        stdout!(true, "created eventloop");

        // Setup a udp socket and join a multicast group.
        let any_addr = "224.1.1.1".parse().expect("Unable to parse address");
        let ip = Ipv4Addr::new(0, 0, 0, 0);
        let addr = SocketAddr::V4(SocketAddrV4::new(ip, 32278));
        let socket = UdpSocket::bound(&addr).expect("Unable to create socket");
        socket.join_multicast(&any_addr).expect("Failed to join multicast");
        stdout!(true, "created udp socket");

        // Setup a udp socket for broadcast
        let ip3 = Ipv4Addr::new(0, 0, 0, 0);
        let add3 = SocketAddr::V4(SocketAddrV4::new(ip3, 32279));
        let socket3 = UdpSocket::bound(&add3).expect("Unable to create socket");
        socket3.set_broadcast(true).expect("Unable to set broadcast");

        // Setup the handler and register it with the event loop
        let mut handler = Udp::new(socket, Receiver);
        stdout!(true, "created handler");
        handler.register(&mut eloop).expect("Unable to register handler with loop");
        stdout!(true, "registered socket");

        // Setup a sync channel
        let (tx, rx) = channel();

        // Spawn a thread to send a couple broadcast messages and shutdown the eventloop.
        thread::spawn(move || {
            thread::sleep(Duration::from_secs(5));
            let ip2 = Ipv4Addr::new(0, 0, 0, 0);
            let add2 = SocketAddr::V4(SocketAddrV4::new(ip2, 11580));
            let socket2 = UdpSocket::bound(&add2).expect("Unable to create socket");
            socket2.set_broadcast(true).expect("Unable to set broadcast");
            // Broadcast a multicast message
            let mcadd = SocketAddr::V4(SocketAddrV4::new(Ipv4Addr::new(224, 1, 1, 1), 32278));
            socket2.send_to(b"Hello, World!", &mcadd).expect("Failed to multicast");
            // I've seen things you people wouldn't believe. Attack ships on fire off the
            // shoulder
            // of Orion. I watched C-beams glitter in the dark near the Tannhauser Gate. All
            // those
            // moments will be lost in time, like tears...in...rain.
            socket2.send_to(b"Time to die", &mcadd).expect("Failed to multicast");
            thread::sleep(Duration::from_secs(10));
            socket2.send_to(b"shutdown", &addr).expect("failed to send shutdown");

            tx.send(0).expect("Unable to send 0");
        });

        thread::spawn(move || {
            thread::sleep(Duration::from_secs(1));
            socket3.send_to(b"foo", &addr).expect("failed to send foo");
            socket3.send_to(b"bar", &addr).expect("failed to send shutdown");
            socket3.send_to(b"baz", &addr).expect("failed to send shutdown");
        });

        // Run the eventloop
        stdout!(true, "running eventloop");
        eloop.run(&mut handler).expect("Unable to run loop");
        rx.recv().expect("Nothing received");
    }
}
