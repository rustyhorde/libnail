//! Nailgun Sender
//! Used for issuing commands to receivers.
use endpoint::{Message, MessageHandler};
use mio;
use std::net::SocketAddr;
use uuid::Uuid;

/// Stuff
pub struct Sender;

impl MessageHandler for Sender {
    fn handle(&self, _: mio::Sender<Message>, uuid: Uuid, message: String, addr: SocketAddr) {
        // chan.send(Message::new(uuid, addr, message)).expect("Unable to send shutdown");
        stdout!(true, "{} sent message {}: {}", addr, uuid, message);
    }
}
